# config file used by 'run' and 'shell'
CONFIG ?= examples/dev.config
# directory paths for 'install'
BINDIR ?= /usr/bin
CONFDIR ?= /etc/osmocom
SYSTEMDUNITDIR ?= /lib/systemd/system/

all: build

build:
	rebar3 compile
	rebar3 escriptize

run: build
	ERL_FLAGS='-config $(CONFIG)' _build/default/bin/osmo-dia2gsup

shell: build
	rebar3 shell --config $(CONFIG)

check:
	rebar3 eunit

analyze:
	rebar3 dialyzer

install: build
	install -Dm0755 _build/default/bin/osmo-dia2gsup \
		$(DESTDIR)$(BINDIR)/osmo-dia2gsup
	install -Dm0644 examples/sys.config \
		$(DESTDIR)$(CONFDIR)/osmo-dia2gsup.config
	install -Dm0644 contrib/systemd/osmo_dia2gsup.service \
		$(DESTDIR)$(SYSTEMDUNITDIR)/osmo_dia2gsup.service

clean:
# Avoid running rebar3 clean if _build doesn't exist, since it would try
# to fetch deps from the Internet and that may not be avaialble when in
# OBS build.
ifneq ("$(wildcard _build/.*)", "")
	rebar3 clean
	rm -rf _build/
else
	rm -f src/diameter_*.erl
endif
