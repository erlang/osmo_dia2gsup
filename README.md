# Osmocom DIAMETER -> GSUP translator

This project implements a proxy translating Diameter into GSUP, hence allowing
Diameter clients connecting to GSUP server. This allows, for instance, using
OsmoHLR as an HSS used by 4G/5G nodes.

## Building

Install erlang and rebar3 packages (not "rebar", that's version 2! You may need
to compile it from source in some distros).

```
$ rebar3 compile
$ rebar3 escriptize
```

## Testing

Unit tests can be run this way:
```
$ rebar3 eunit
```

## Running

Once osmo\_dia2gsup is built, you can start it this way:

```
$ rebar3 shell
```

Passing a config file:

```
$ rebar3 shell --config ./examples/sys.config
```

Running it as an escript:

```
$ rebar3 escriptize
$ ERL_FLAGS='-config ./examples/sys.config' _build/default/bin/osmo-dia2gsup
```

## Build a new release

rebar3 release

